package com.example.archivosbdpersonal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

	}

	public void buscar(View vista) {
		String estado = Environment.getExternalStorageState();
		if (!estado.equals(Environment.MEDIA_MOUNTED)) {
			Toast.makeText(this, "NO HAY SD CARD...", Toast.LENGTH_LONG).show();
			finish();
		}
		try {
			File dir = Environment.getExternalStorageDirectory();
			File pt = new File(dir.getAbsolutePath() + File.separator
					+ "dbPersonal.txt");
			BufferedReader lee = new BufferedReader(new FileReader(pt));
			String linea = "";
			String carnet = ((EditText) findViewById(R.id.EditText1)).getText()
					.toString();
			while ((linea = lee.readLine()) != null) {
				String datos[] = linea.split(";");
				if (datos[4].equals(carnet)) {
					llenarDatos(datos);
					return;
				}
			}
			Toast.makeText(this, "Personal no encontrado", Toast.LENGTH_LONG)
					.show();

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void mostrarCantidadAportes() {
		String estado = Environment.getExternalStorageState();
		if (!estado.equals(Environment.MEDIA_MOUNTED)) {
			Toast.makeText(this, "NO HAY SD CARD...", Toast.LENGTH_LONG).show();
			finish();
		}
		try {
			File dir = Environment.getExternalStorageDirectory();
			File pt = new File(dir.getAbsolutePath() + File.separator
					+ "dbAportes.txt");
			BufferedReader lee = new BufferedReader(new FileReader(pt));
			String linea = "";
			int cantidadAportes = 0;
			int numeroDeAportes = 0;
			String carnet = ((EditText) findViewById(R.id.EditText1)).getText()
					.toString();
			while ((linea = lee.readLine()) != null) {
				String datos[] = linea.split(";");
				if (datos[0].equals(carnet)) {
					cantidadAportes += (Integer.parseInt(datos[1]));
					numeroDeAportes++;
				}
			}
			((EditText) findViewById(R.id.EditText6)).setText(numeroDeAportes
					+ "");
			((EditText) findViewById(R.id.EditText7)).setText(cantidadAportes
					+ "");

		} catch (Exception e) {
			// TODO: handle exception
		}
	}

	public void aportes(View vista) {
		String carnet = ((EditText) findViewById(R.id.EditText1)).getText()
				.toString();
		Bundle miBundle = new Bundle();
   	 	miBundle.putString("carnet", carnet);
		Intent vd = new Intent(this, Aportes.class);
	   	vd.putExtras(miBundle);//Le enviamos el bundle al intent (a la vista 'vd')
    	startActivity(vd);


	}

	private void llenarDatos(String[] datos) {
		EditText dato2 = (EditText) findViewById(R.id.EditText2);
		EditText dato3 = (EditText) findViewById(R.id.EditText3);
		EditText dato4 = (EditText) findViewById(R.id.EditText4);
		EditText dato5 = (EditText) findViewById(R.id.EditText5);
		dato2.setText(datos[0]);
		dato3.setText(datos[1]);
		dato4.setText(datos[2]);
		dato5.setText(datos[3]);

		mostrarCantidadAportes();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
