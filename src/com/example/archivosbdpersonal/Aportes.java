package com.example.archivosbdpersonal;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

import android.app.Activity;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class Aportes extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_aportes);

		mostrarAportes();
	}
	
	public void mostrarAportes() {
		String estado = Environment.getExternalStorageState();
		if (!estado.equals(Environment.MEDIA_MOUNTED)) {
			Toast.makeText(this, "NO HAY SD CARD...", Toast.LENGTH_LONG).show();
			finish();
		}
		try {
			Bundle miBundle = this.getIntent().getExtras();
			String carnet = miBundle.getString("carnet");
			
			File dir = Environment.getExternalStorageDirectory();
			File pt = new File(dir.getAbsolutePath() + File.separator
					+ "dbAportes.txt");
			BufferedReader lee = new BufferedReader(new FileReader(pt));
			String linea = "";
			int cantidadAportes = 0;
			int numeroDeAportes = 0;
			String filas = "";

			while ((linea = lee.readLine()) != null) {
				String datos[] = linea.split(";");
				if (datos[0].equals(carnet)) {
					filas+= datos[1] + "\t\t" +datos[2]+ "\n";
					cantidadAportes += (Integer.parseInt(datos[1]));
					numeroDeAportes++;
				}
			}
			filas+= "Cantidad: "+numeroDeAportes +"\t\t Total: "+cantidadAportes;
			((EditText)findViewById(R.id.editText1)).setText(carnet);
			((TextView) findViewById(R.id.textView3)).setText(filas);
		} catch (Exception e) {
			// TODO: handle exception
		}
	}
	
    public void finalizar(View vista){
    	
    	finish();//Esta instruccion finaliza el hilo, cada vista debe tener una de estas
    }

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.aportes, menu);
		return true;
	}

}
